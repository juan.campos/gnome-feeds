from gfeeds.main_leaflet import MainLeaflet
from gfeeds.confManager import ConfManager
from gfeeds.feeds_manager import FeedsManager
from gfeeds.base_app import BaseWindow, AppShortcut
from datetime import datetime


class GFeedsAppWindow(BaseWindow):
    def __init__(self, application):
        self.confman = ConfManager()
        self.feedman = FeedsManager()
        self.app = application

        self.leaflet = MainLeaflet()

        super().__init__(
            app_name='Feeds',
            icon_name='org.gabmus.gfeeds',
            shortcuts=[
                AppShortcut(
                    'F10', lambda *_:
                        self.leaflet.left_headerbar.menu_btn.popup()
                ),
                AppShortcut(
                    '<Control>r', self.feedman.refresh
                ),
                AppShortcut(
                    '<Control>f', lambda *_:
                        self.leaflet.left_headerbar.search_btn.set_active(
                            not
                            self.leaflet.left_headerbar.search_btn.get_active()
                        )
                ),
                AppShortcut(
                    '<Control>j', self.leaflet.sidebar.select_next_article
                ),
                AppShortcut(
                    '<Control>k', self.leaflet.sidebar.select_prev_article
                ),
                AppShortcut(
                    '<Control>plus', self.leaflet.webview.key_zoom_in
                ),
                AppShortcut(
                    '<Control>minus', self.leaflet.webview.key_zoom_out
                ),
                AppShortcut(
                    '<Control>equal', self.leaflet.webview.key_zoom_reset
                ),
                AppShortcut(
                    '<Control>t', lambda *_:
                        self.leaflet.filter_flap.set_reveal_flap(
                            not self.leaflet.filter_flap.get_reveal_flap()
                        )
                )
            ]
        )

        self.append(self.leaflet)

        self.confman.connect(
            'dark_mode_changed',
            lambda *_: self.set_dark_mode(self.confman.conf['dark_mode'])
        )
        self.set_dark_mode(self.confman.conf['dark_mode'])

    def present(self):
        super().present_with_time(int(datetime.now().timestamp()))
        self.set_default_size(
            self.confman.conf['window-width'],
            self.confman.conf['window-height']
        )

    def emit_destroy(self, *_):
        self.emit('destroy')

    def on_destroy(self, *_):
        self.leaflet.sidebar.listview_sw.shutdown_thread_pool()
        self.confman.conf['window-width'] = self.get_width()
        self.confman.conf['window-height'] = self.get_height()
        # cleanup old read items
        feeds_items_ids = [
            fi.identifier for fi in self.feedman.article_store.list_store
        ]
        to_rm = []
        for ri in self.confman.conf['read_items']:
            if ri not in feeds_items_ids:
                to_rm.append(ri)
        read_items: list = self.confman.conf['read_items']
        for ri in to_rm:
            read_items.remove(ri)
        self.confman.conf['read_items'] = read_items
        self.confman.save_article_thumb_cache()
